base_x=75;
base_y=40;
base_z=10;
wall=2.2;
corr=0.2;
pillar_xy=wall*1.9;
pillar_z=wall*4;
roof_z=5;
window_x=28;
window_z=roof_z;
eje_r=5.8/2;
$fn=60;

eje();
//adaptador();

module eje(){
    cylinder(r=eje_r,h=base_y-wall);
    }
module adaptador(){
   
       difference(){
      union(){
       cube([ base_x, base_y, base_z ]);
            //roof
   color("green") translate([ 0.00, 0.00, base_z ]) cube([ base_x, base_y, roof_z ]);
 color("blue") translate([ -wall, -wall, 0.00 ])  cube([ base_x-corr+wall*2, base_y-corr+wall*2, base_z ]);
      }
        color("red")  translate([ wall, wall,-corr ])  cube([ base_x-wall*2-corr, base_y-wall*2-corr, base_z+roof_z+corr ]);
              translate([ 11.1, -corr,base_z ])  cube([ window_x, base_x, window_z ]);
 //espacio interno de union
            translate([ 0, 0, -wall ])  cube([ base_x+corr, base_y+corr, base_z ]);
       }
        
       //pillars
         translate([ wall+corr, wall+corr, base_z-wall ]) cube([ pillar_xy, pillar_xy, pillar_z+wall*2 ]);
     
       color("red") translate([ wall, base_y-wall-pillar_xy-corr, base_z-wall ]) cube([ pillar_xy, pillar_xy, pillar_z+wall*2 ]);
        color("white") translate([ base_x-wall-pillar_xy, base_y-wall-pillar_xy, base_z-wall ]) cube([ pillar_xy, pillar_xy, pillar_z+wall*2 ]);
         color("green")  translate([ base_x-wall-pillar_xy, wall, base_z-wall ]) cube([ pillar_xy, pillar_xy, pillar_z+wall*2 ]);
       }