# BrewerMicro

Microscopio para monitoreo y conteo de levaduras para la elaboración de cervezas y para fermentaciones en general.   
[English version](/Readme_EN.md)  

<img src="/Prototipo_Actual/Imagenes/IMG_20200819_080843.jpg" width="400"/>
<img src="/Prototipo_Actual/Imagenes/50242738831_982df677ed_o.jpg" width="400"/>
<img src="/Prototipo_Actual/Imagenes/50242955947_8535cbd765_o.jpg" width="400"/>
<img src="/Prototipo_Actual/Imagenes/usaf2.jpg" width="400"/>

Con este proyecto buscamos desarrollar y caracterizar un microscopio de bajo costo DIY que los/as cerveceros/as artesanales puedan utilizar para contar y evaluar la viabilidad de levaduras para su uso en producción.  
<details>
  <summary markdown="span">Breve historia del proyecto</summary>
El proyecto comenzó a partir del Taller Open Hardware Valdivia 2018 y el Taller de Ciencia y Cerveza dictado por [IPATEC](https://ipatec.conicet.gov.ar/ciencia-cerveza/) en Valdivia durante Marzo 2019 (ambos eventos finaciados por el Instituto Milenio iBio). 
Para este proyecto tuvimos una doble motivación, por un lado, la necesidad concreta de ayudar a los cervecerxs a dar un paso mas en su autonomía microbiológica facilitando la observación, el conteo y la evaluación de la levaduras para mejorar las reutilizaciones en fábrica y, por otro lado, el deseo de aprender acerca de la adopción de estas herramientas científicas abiertas fuera del ámbito acadmémico/científico. 
El desarrollo ha seguido una metodología de colaboración y prototipado constante que buscado mediante talleres involucrar lo antes posible a los potenciales usuarios/colaboradores en este proceso de co-creación. Hemos realizado talleres en Valdivia, Bariloche y una breve charla en Mendoza. Actualmente estamos preparando un taller on-line con gente de distintos perfiles de todo Chile.
Un hito importante de este proceso ha sido la adpoción de algunas ideas surgidas de este proceso por parte de IPATEC para su [primer versión de microscopio](/Prototipo%20IPATEC%201.0).  
</details>

Nuestros diseños se han inspirado y son hibridos basados en 

<details>
  <summary markdown="span">Otros microscopios open-source</summary>
- [DIY microscope](https://hackteria.org/wiki/DIY_microscopy)   
- [Openflexure](https://github.com/rwb27/openflexure_microscope)      
- [Flypi](https://github.com/amchagas/Flypi)  
- [Fluopi](https://osf.io/dy6p2/)  
- [Comunicaciones Especulativas](http://interspecifics.cc/comunicacionesespeculativas/)  
</details>

## Prototipo Actual

<img src="/Prototipo_Actual/Imagenes/IMG_20200819_081307.jpg" width="600"/>  

El prototipo actual esta basado en una camara web Logitech C270 y una base impresa en 3D para alojar y enfocar un lente M12. El diseño tipo tornillo de este manipulador se ha inspirado en el trabajo de [Tristan-Landin et al. (2019)](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0215114). La iluminación es una adaptación de la que utiliza el [Openflexure](https://github.com/rwb27/openflexure_microscope).   
Con este prototipo hemos podido ver, y distinguir claramente, celulas de levadura vivas (sin teñir) y células muertas (teñidas con azul de metileno).   
También puede ser utilizado por otros fermentadores para observar sus producciones (ej. kombucha, vino, chicha, kefir, kimchi)
    [Ver más imágenes](https://www.flickr.com/photos/144912655@N02/albums/72157710829687746) 

<details>
  <summary markdown="span">:nut_and_bolt:  Si quieres replicar este prototipo</summary>
En esta [carpeta](/Prototipo_Actual) pueden encontrar la [lista de materiales](BOM.csv), los archivos .stl y una [guia](Guia_ensamblado.md) con indicaciones para la impresión y el ensamblado de las partes. 
</details>

<details>
  <summary markdown="span">:microscope: Si quieres usar este prototipo</summary>
Se puede utilizar el microscopio con cualquier software que reconozca la cámara web Logitech C270.    
Esta son algunos softwares sugeridos para pc:  

- Linux -> Cheese.  
    Desde el terminal pueden descargar e instalar con:  
    sudo apt-get install cheese  

- Windows-> https://www.logitech.com/es-ar/product/capture
- Mac -> https://www.logitech.com/en-us  

  En caso de que quieran usar su teléfono celular como display del microscopio necesitan:  
- Un cable OTG a micro USB (link) 
- Descargar la App [USB Camera] (https://play.google.com/store/apps/details?id=infinitegra.app.usbcamera)

Para saber como preparar y diluir muestras de levadura recomendamos consultar el curso virtual [**Autonomía Microbiológica, un camino a la sustentabilidad cervecera**](https://ipatec.conicet.gov.ar/cursos/) de IPATEC

Para el análisis de la cantidad y calidad de levadura en la producción cervecera recomendamos la aplicación [MicroBrew 2.0](https://play.google.com/store/apps/details?id=ar.com.innqube.microbrewar&hl=es_AR) desarrollada por IPATEC. 

</details>

<details>
  <summary markdown="span">:wrench: Si quieres modificar este prototipo</summary>
El diseño ha sido realizado con el software libre [OpenScad](www.openscad.org). 
En la carpeta [Openscad](/Prototipo_Actual/Openscad) pueden encontrar el archivo fuente del diseño, una [guía](/Prototipo_Actual/Openscad/Readme.md) de cómo está organizado y algunas indicaciones acerca de los parámetros y las librerías utilizadas. 
</details>

## Estado de desarrollo

Podes consultar los [issues](https://gitlab.com/nanocastro/microbrew/issues) para saber en qué cosas estamos trabajando para mejorar este diseño.  
Actualmente estamos buscando trabajar junto a cervecerxs locales para comenzar a construir una base de imagenes común que permita implementar métodos de conteo automático de células. 

## Prototipos anteriores

Hemos evaluado y documentado varias combinaciones de cámaras, ópticas, manipuladores e iluminación. Los diseños y las evaluaciones de estos primeros prototipos se pueden encontrar [aquí](/Early_prototypes/Prototipos.md)

## Cómo participar y colaborar?

Podes escribirnos a brewermicro2020 at gmail.com para que respondamos a tu inquietud.  
También podes hacer un fork de este proyecto y comenzar a subir tus modificaciones.

### Talleres

Hemos realizado algunos [talleres](/Workshops) para cerveceros y público en general en Argentina (Bariloche) y Chile (Santiago, Valdivia) con el objetivo de sumar colaboradores y usuarios. 

## Financiamientos recibidos

<details>
  <summary markdown="span">Biomaker Challenge 2019</summary>
El proyecto ha sido financiado por el [Cambridge-Norwich Biomaker Challenge 2019](https://www.biomaker.org/cambridge-norwich-challenge).   
[Documentación y los informes](/Biomaker2019) del proyecto Biomaker.   
También se puede consultar la documentación en [hackster](https://www.hackster.io/nanocastro/brewermicro-diy-microscope-for-counting-yeast-f78f19)   
Agradecemos especialmente a Sam Mugford,  Lawrence Percival-Alwyn, Jenny Molloy y Jim Hasseloff por su ayuda durante el desarrollo del proyecto.  
</details>
<details>
  <summary markdown="span">iBio</summary>
Instituto Milenio iBio – Iniciativa Científica Milenio MINECON
</details>

> > > 
> > 
> > Este proyecto es una colaboración entre:

- [Fernan Federici - PUC - Chile](https://federicilab.org/about/)

- [Gudrun Kausel - UACH - Chile](http://www.ciencias.uach.cl/instituto/bioquimica_microbiologia/investigacion/gudrun_kausel.php)

- [Fernando Castro - Independent - Argentina](https://gitlab.com/nanocastro/)

- [Carlos Bertoli - IPATEC - Argentina](https://ipatec.conicet.gov.ar/ciencia-cerveza/)

- tú? (este es un proyecto abierto y extendemos la invitación a cualquier persona interesada en participar)
  
  > > > 

  Este proyecto se publica licenciado bajo la [licencia de open hardware](Licencia.txt) de CERN.
