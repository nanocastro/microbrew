# BM4_USBWebCam_M12lens

This setup is basically the M12 lens tube of the OpenFlexure for the 8Mp USB webcam

<img src="/Early_prototypes/BM4_USBWebCam_M12lens/Images/BM4_USBWebCam_M12lens_1.jpg" width="400"/> <img src="/BM4_USBWebCam_M12lens/Images/BM4_USBWebCam_M12lens.jpg" width="400"/>

### BOM

| **Part**         | **Component**                      | **Link**                                                                                                                                                                                             |
|:----------------:|:----------------------------------:|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|
| Camera           | 8Mp USB web camera                 | [Cameras](/Early_prototypes/Parts/cameras.md)                                                                                                                                                                         |
| Optics           | M12 - 2.8mm                        | [Optics](/Early_prototypes/Parts/optics.md)                                                                                                                                                                           |
| Illumination     | simple led light                   |                                                                                                                                                                                                      |
| Manipulator/base | manual                             |                                                                                                                                                                                                      |
| Lens tube        | Openflexure M12 lens tube for C270 | [M12 Lens tube](/Early_prototypes/Stl/M12-optics.stl) |

### Images

The images below correspond to an experiment with several M12 lenses

Breadyeast - M12 - 2.8mm    
<img src="/Early_prototypes/BM4_USBWebCam_M12lens/Images/BM4_USBWebCam_M12-2.8_breadyeast.jpg" width="400"/> <img src="/Early_prototypes/BM4_USBWebCam_M12lens/Images/BM4_USBWebCam_M12-2.8_breadyeast2.jpg" width="400"/>  
Breadyeast - M12 - 3.6mm    
<img src="/Early_prototypes/BM4_USBWebCam_M12lens/Images/BM4_USBWebCam_M12-3.6_breadyeast1.jpg" width="400"/> <img src="/Early_prototypes/BM4_USBWebCam_M12lens/Images/BM4_USBWebCam_M12-3.6_breadyeast2.jpg" width="400"/>  
Breadyeast - M12 - 6mm    
<img src="/Early_prototypes/BM4_USBWebCam_M12lens/Images/BM4_USBWebCam_M12-6_breadyeast1.jpg" width="400"/>  
