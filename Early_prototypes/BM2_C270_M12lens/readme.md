# BM2_C270_M12lens

This setup is a combination of the C270 logitech webcam, the Flypi manipulator, a cylindrical adaptor for M12 lenses  and the OpenFlexure illumination. 

![BM2](/Early_prototypes/BM2_C270_M12lens/Images/ValdiviaTest/BM_c270_M12-2.5.png)

### BOM

| **Part**         | **Component**                                   | **Link**                               |
|:----------------:|:-----------------------------------------------:|:--------------------------------------:|
| Camera           | Logitech C270 webcam                            | [Cameras](/Parts/cameras.md)           |
| Optics           | M12 lens of different focal distance            | [Optics](/Parts/optics.md)             |
| Illumination     | adaptation from OpenFlexure                     | [Illumination](/Parts/Illumination.md) |
| Manipulator/base | adaptation from Flypi to test different cameras | [Manipulator](/Parts/Manipulator.md)   |
| M12 lens holder  | cylindrical M12 lens adaptor                    |                                        |

### Images

The images below correspond to two different experiments with several M12 lenses.

#### During DIY Microscopy for brewers workshop (Valdivia/Chile)

Breadyeast - M12 - 2.5mm

![BM2](/Early_prototypes/BM2_C270_M12lens/Images/ValdiviaTest/BM_c270_M12-2.5_breadyeast2019-06-15-1.png)

Breadyeast - M12 - 3.6mm

![BM2](/Early_prototypes/BM2_C270_M12lens/Images/ValdiviaTest/BM_c270_M12-3.6_breadyeast2019-06-15-3.png)

Breadyeast - M12 - 6mm

![BM2](/Early_prototypes/BM2_C270_M12lens/Images/ValdiviaTest/BM_c270_M12-6_breadyeast2019-06-15-5.png)

Breadyeast - M12 - 12mm

![BM2](/Early_prototypes/BM2_C270_M12lens/Images/ValdiviaTest/BM_c270_M12-12_breadyeast2019-06-15-8.png)

#### During visit at IPATEC (Bariloche/Argentina)

<img src="/Early_prototypes/BM2_C270_M12lens/Images/IPATECTest/20190617_113841.jpg" width="400"/> <img src="/Early_prototypes/BM2_C270_M12lens/Images/IPATECTest/20190617_113855.jpg" width="400"/>

phaffia rhodozyma (with methylene blue) - M12 - 2.5mmm    
<img src="/Early_prototypes/BM2_C270_M12lens/Images/IPATECTest/BM_c270_M12-2.5_phaffia_IPATEC2019-06-17-12h47m29s066.png" width="400"/> <img src="/Early_prototypes/BM2_C270_M12lens/Images/IPATECTest/BM_c270_M12-2.5_phaffia_IPATEC2019-06-17-12h50m25s836.png" width="400"/>  
phaffia rhodozyma (with violet methylene) - M12 - 2.5mmm  
<img src="/Early_prototypes/BM2_C270_M12lens/Images/IPATECTest/BM_c270_M12-2.5_phaffia_IPATEC_violetametileno_diaf2019-06-17-17h25m13s560.png" width="400"/> <img src="/Early_prototypes/BM2_C270_M12lens/Images/IPATECTest/BM_c270_M12-2.5_phaffia_IPATEC_violetametileno_diaf2019-06-17-17h02m29s125.png" width="400"/>  
Images with commercial microscope 40x lens  (pictures taken with [cellphone adaptor](/Parts/phoneadaptor/Soporte_micro_2.0.stl))  
phaffia rhodozyma (with methylene blue)  
<img src="/Early_prototypes/BM2_C270_M12lens/Images/IPATECTest/MP_40x_phaffia_azulmetileno.jpg" width="400"/> <img src="/Early_prototypes/BM2_C270_M12lens/Images/IPATECTest/MP_40x_phaffia_azulmetileno_b.jpg" width="400"/>  
phaffia rhodozyma (with violet methylene)  
<img src="/Early_prototypes/BM2_C270_M12lens/Images/IPATECTest/MP_40x_phaffia_violetametileno.jpg" width="400"/> 
