# Optics

## M12 lens

![M12lens](/Early_prototypes/Parts/Images/m12.png)

M12 Lenses, also referred to as S-Mount Lenses or Micro-Video Lenses, are board lenses used to focus an image onto a camera sensor but if they are used in an inverted way can be used as magnifiers.  

They are tipically used on security cameras. The name M12 lens comes from the size of the mount that is used on these lenses. They use an M12 x 0.5mm thread to attach to the camera.  

The lens nomenclature is M12-<focal-distance> i.e.  M12-6mm is an M12 lens of focal distance 6mm.  

The f-number on M12 lens represents the size of the aperture and therefore how much light can enter the lens. M12 lenses usually have a fixed aperture and therefore a constant f-number. A smaller f-number means a larger aperture and more light can reach the sensor.   

Example: [PT-3621 3.6mm, F2.1 Board Lens](http://www.m12lenses.com/3-6mm-F2-1-Board-Lens-p/pt-3621.htm)

We are using :

2.5 mm: [PT-02520 2.5mm F2.5](http://www.m12lenses.com/2-5mm-F2-5-Board-Lens-p/pt-02520.htm) (USD 13.2)

2.8 mm: [PT-02820 2.8mm, F2.0 Board Lens](http://www.m12lenses.com/2-5mm-F2-5-Board-Lens-p/pt-02520.htm)(USD 6.25)

3.6 mm: [PT-03621 3.6mm F2.1](http://www.m12lenses.com/3-6mm-F2-1-Board-Lens-p/pt-3621.htm) (USD 4)

6 mm: [PT-0620 6mm F2.0](http://www.m12lenses.com/6-0mm-F2-0-Board-Lens-p/pt-0620.htm) (USD 4)

12 mm: [PT-1220 12mm F2.0](http://www.m12lenses.com/12mm-F2-0-Board-Lens-p/pt-1220.htm) (USD 6.25)

## RMS objectives

<img src="/Early_prototypes/Parts/Images/rms2.png" width="400"/>

RMS stands for Royal Microscopical Society and refers to the standard mounting threads on almost all microscope objectives.

This is an image of how to recognize different objectives 

![Objectives](/Early_prototypes/Parts/Images/rms-obj.png)

Nomenclature:

 rms-<magnificacion>-<infinite or 160> 

Example: rms-40x-160 is an 40x magnification objective of 160mm focal distance, see below for details)

### RMS-40x-160 (finite objective)

Finite microscope objectives are designed to project a diffraction-limited image at a fixed plane (the **intermediate** image plane), which is dictated by the microscope tube length and located at a pre-specified distance from the rear focal plane of the objective.

The RMS-40x-160 objective can be sourced from [cnscope](http://www.cnscope.com/index.php?route=product/product&path=59_62&product_id=59) or [Alibaba](http://www.aliexpress.com/item/40X-L-195-Plan-Achromatic-Biological-Microscope-Objective-Lens-Laboratory-Biomicroscopy-Accessories-20-2mm-for-Medical/32600189317.html?spm=2114.40010208.4.32.A7pVmT) 

#### Characteristics:

● 40x

● RMS mount (universal thread)

● 160 mm focus (it is not infinity focus → not collimated)

● NA 0.6-0.8 (numerical aperture)

As the objective is 160 needs a [focal lens from Thorlabs]([https://www.thorlabs.com/thorproduct.cfm?partnumber=AC127-050-A](https://www.thorlabs.com/thorproduct.cfm?partnumber=AC127-050-A) to focus on the camera sensor. 

#### Use

You can see how the optics are assembled on this [OpenFlexure guide](https://openflexure.org/projects/microscope/docs/2b_high_resolution_optics_module.html)

### Infinite objectives

Most manufacturers have now transitioned to infinity-corrected objectives that project emerging rays in parallel bundles from every azimuth to infinity. 

[100x (oil)](http://www.cnscope.com/index.php?route=product/product&path=59_61&product_id=74) from cnscope

These objectives require a tube lens in the light path to bring the image into focus at the camera sensor.
