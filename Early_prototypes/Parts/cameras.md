# Cameras

All prototypes are based on the [simple hack ](https://hackteria.org/wiki/DIY_microscopy) of inverting the lens of a commercial camera to magnify the image.

## Logitech c270 Webcam

<img src="/Early_prototypes/Parts/Images/C270.jpg" width="200"/> 

#### Commercial link:

Chile: https://www.pcfactory.cl/producto/1007-logitech-webcam-c270-hd
UK: 

#### Characteristics:

- Resolution: 1280x720

- Pixel Size: 2.8um

- Sensor Size: 3.58x2.02mm

- Stock lens focal length: 4.2mm  

#### Use

This camera is conected directly on the USB port of your computer. You can use a software as VLC to get the images. 

This is a set of instructions on how to disasemble and modify the C270 (from OpenFlexure)

https://gitlab.com/rwb27/openflexure-microscope/blob/master/design_files/Butchering%20a%20C270%20webcam%20&%20adding%20an%20LED.pdf

## 8Mp USB web camera

<img src="/Early_prototypes/Parts/Images/usb8mp.png" width="200"/> 

#### Commercial link:

http://www.webcamerausb.com/elp-8mp-highdefinition-usb-camera-module-usb20-sony-imx179-color-cmos-sensor-75degree-lens-p-45.html

#### Characteristics:

- Model    ELP-USB8MP02G-L75

- Sensor    SONY IMX179

- Lens Size    1/3.2inch

- Pixel Size    1.4um

- image area    6.18mm x 5.85mm

#### Use

This camera is conected directly on the USB port of your computer. You can use a software as VLC to get the images. 

## Sport-camera 4k (Drograce wp350)

<img src="/Early_prototypes/Parts/Images/drograce.jpg" width="200">

#### Commercial link:

[https://www.amazon.com/DROGRACE-Camera-Action-Waterproof-Accessories/dp/B01N311GR0](https://www.amazon.com/DROGRACE-Camera-Action-Waterproof-Accessories/dp/B01N311GR0)

#### Characteristics:

Model    Action Camera 4K
Optical Sensor Resolution    12 Megapixels

#### Use

Disasembly to invert lens

<img src="/Early_prototypes/Parts/Images/20190604_113619.jpg" width="400"/> <img src="/Parts/Images/20190604_121323.jpg" width="400"/>

Can be used  with an [Android app](https://play.google.com/store/apps/details?id=com.chiphd.xly.view&hl=es_CL)
