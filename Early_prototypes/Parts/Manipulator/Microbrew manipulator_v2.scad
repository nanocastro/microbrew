include <Cubic_Bearing.scad>

////bearing(Inside_Diameter,Outside_Diameter,Length,Wall_Thickness,Number_of_teeth);
Inside_Diameter = 4;
Outside_Diameter = 10;
Length = 12;
Wall_Thickness = 1.5;
Number_of_teeth = 8;

/// PARAMETERS FOR THE CUSTOMIZER

wall = 3;

//X_range = 44; // Length(70) - B_X(20) - 2*wall(6) = 44
//Y_range = 35; // Width(70) - C_Y(23) - 4*wall(12) = 38
//Z_range = 30; // C_Topheight(40) - D_Z(12) - 2*Wall(6) = 22

Radius_of_holes = 2.1;
Width_of_boat_slids = 7.4;
Height_of_boat_slids = 3.2;

R_main_extra = 0.2; // extra radius for non-guiding holes
R_main_head = 4; // radius of screw head for part C
tol=0.5;
R_h_agarre=1.6;

//translate([0,0,(Outside_Diameter+wall)/2])cube([2*Outside_Diameter+wall,2*Outside_Diameter,wall],center=true);//base 


base_trans();


Y_ext=Outside_Diameter*3+2*wall+tol;
X_ext=2.5*(Length+wall)+2*wall;
Z_ext=Outside_Diameter+2*wall+tol;
Y_int=Outside_Diameter*3+tol;
X_int=2.5*(Length+wall);

//base_fix();
module base_fix(){
difference(){
union(){
translate([0,0,-wall-2*tol])
difference(){
cube ([X_ext,Y_ext,Z_ext],center=true);
    cube([X_int,Y_int,Z_ext+wall],center=true);
}
translate([X_int/2,-Outside_Diameter,0])rotate([0,90,0])cylinder(h=wall,r=Radius_of_holes+3*tol,center=true,$fn=20);
translate([-X_int/2,-Outside_Diameter,0])rotate([0,90,0])cylinder(h=wall,r=Radius_of_holes+3*tol,center=true,$fn=20);
translate([-X_int/2,Outside_Diameter,0])rotate([0,90,0])cylinder(h=wall,r=Radius_of_holes+3*tol,center=true,$fn=20);
translate([X_int/2,Outside_Diameter,0])rotate([0,90,0])cylinder(h=wall,r=Radius_of_holes+3*tol,center=true,$fn=20);
translate([0,0,-Outside_Diameter-3/2*tol])#cube([X_ext,2*Outside_Diameter,wall],center=true);//base 

}

#translate([0,0,-Outside_Diameter/2])rotate([0,90,0])cylinder(h=X_ext+2*wall,r=Radius_of_holes,center=true,$fn=20);//tornillo 
translate([X_int/2-wall/2,-Outside_Diameter,wall])cube([wall,3*Radius_of_holes+3*tol,2*wall],center=true);
translate([X_int/2-wall/2,Outside_Diameter,wall])cube([wall,3*Radius_of_holes+3*tol,2*wall],center=true);
#translate([0,Outside_Diameter,0])rotate([0,90,0])cylinder(h=X_ext-2*wall,r=Radius_of_holes,center=true,$fn=20);//guia 
#translate([0,-Outside_Diameter,0])rotate([0,90,0])cylinder(h=X_ext-2*wall,r=Radius_of_holes,center=true,$fn=20);//guia 
}
}


module base_trans()
{
    difference(){
    union(){
    translate([0,0,0])cube([Length+wall,Outside_Diameter,Outside_Diameter], center = true);//cubo central
 translate([0,Outside_Diameter,0]) rotate([0,90,0])bearing (Inside_Diameter,Outside_Diameter,Length+wall,Wall_Thickness,Number_of_teeth);//bearing cubico izq
 translate([0,-Outside_Diameter,0]) rotate([0,90,0])bearing(Inside_Diameter,Outside_Diameter,Length+wall,Wall_Thickness,Number_of_teeth);//bearing cubico der
        translate([-(Length+wall)/2,0,-Outside_Diameter])rotate([0,90,0]) cylinder(Length+wall,Outside_Diameter,Outside_Diameter,$fn=3);//defasaje para acople de tornillo      
//    #translate([0,-Outside_Diameter,Outside_Diameter/2+wall/2+R_h_agarre/2])cube([Length+wall,wall,wall/2+2*R_h_agarre],center=true);//agarre etapa superior
//        #translate([0,Outside_Diameter,Outside_Diameter/2+wall/2+R_h_agarre/2])cube([Length+wall,wall,wall/2+2*R_h_agarre],center=true);//agarre etapa superior   
        
         }
    translate([0,0,-Outside_Diameter*3/2])rotate([90,0,0])cube([Length+2*wall,Outside_Diameter+wall,Outside_Diameter+wall+1], center = true);
#translate([-wall,0,-Outside_Diameter])cube([Boatslot_height,Boatslot_width,2*Outside_Diameter], center = true); //tuerca
#translate([wall,0,-Outside_Diameter])cube([Boatslot_height,Boatslot_width,2*Outside_Diameter], center = true); //tuerca
#translate([0,0,-Outside_Diameter/2])rotate([0,90,0])cylinder(h=Length+2*wall,r=Radius_of_holes,center=true,$fn=20);//tornillo 
}
}


Boatslot_width = Width_of_boat_slids; // how wide so that an M4 doesnt rotate?
Boatslot_height = Height_of_boat_slids; // how thick is an M4 boat?

//nuttrap();
module nuttrap(){
translate([0,(c+1)/2,0])rotate([90,0,0])hexagon(c+1,(b+1)/2);
translate([0,0,(b*3)/2])cube([b+1,c+1,b*3],center = true);
}

module reg_polygon(sides,radius)
{
  function dia(r) = sqrt(pow(r*2,2)/2);  //sqrt((r*2^2)/2) if only we had an exponention op
  if(sides<2) square([radius,0]);
  if(sides==3) triangle(radius);
  if(sides==4) square([dia(radius),dia(radius)],center=true);
  if(sides>4) circle(r=radius,$fn=sides);
}

module hexagonf(radius)
{
  reg_polygon(6,radius);
}

module hexagon(height,radius) 
{
  linear_extrude(height=height) hexagonf(radius);
}
