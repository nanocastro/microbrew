# Manipulator

The manipulator used on BM2 and BM3 is an adaptation of the [Flypi manipulator]([https://github.com/amchagas/Flypi](https://github.com/amchagas/Flypi) that was originally adapted from the disign of Tim Marzullo

![Manipulator](/Parts/Manipulator/manipulator.png)

To assembly the manipulator you need:

- [Manipulator](/Parts/Manipulator/Stl/Microbrew manipulator_v1.stl)  

- [Knobs](/Parts/Manipulator/Stl/knob.stl)   

- [C270 base](/Parts/Manipulator/Stl/base_C270.stl)

- M4 nuts and bolts (see assembly guide below)

[Instructions to assembly the manipulator](%5Bhttps://openlabwaredotnet.files.wordpress.com/2015/11/manipulator-assembly-instructions.pdf) (slightly different for this version)

You can find the Openscad files [here](/Parts/Manipulator/)
