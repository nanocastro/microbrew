myLength = 30;
gapBuffer = 2;
holeSize = 3.2;
holeBuffer =2;
tabLength = holeSize+holeBuffer*2;
tabExtra = 2;
tabWidth = 4;


module straight(x = 0, y = 0, length = 30){
	translate ([x, y]){
		difference (){
			translate ([-length/2, -(tabWidth+gapBuffer*2)/2]) cube ([length,tabWidth+gapBuffer*2,holeSize+holeBuffer*2]);
			translate([-length/2-1,tabWidth/-2, -0.5]) cube ([tabLength+1+tabExtra,tabWidth, holeSize+holeBuffer*2+1]);
			translate([-length/2+tabLength/2,-tabWidth-1, (holeSize+holeBuffer*2)/2]) rotate ([-90 ,0]) cylinder (tabWidth+gapBuffer*2+2, holeSize/2, holeSize/2, $fn=30);
        translate([-length/2+tabLength+2*gapBuffer,gapBuffer,0])  cube([length+1,gapBuffer+1,holeSize+holeBuffer*2+1]);
   
      translate([-length/2+tabLength+2*gapBuffer,-gapBuffer*2,0])  cube([length+1,gapBuffer,holeSize+holeBuffer*2+1]);

            	}
		difference(){
		translate([length/2,tabWidth/-2]) cube ([tabLength,tabWidth, holeSize+holeBuffer*2]); 
			translate([length/2+tabLength/2,-tabWidth-1, (holeSize+holeBuffer*2)/2]) rotate ([-90 ,0]) cylinder (tabWidth+gapBuffer*2+2, holeSize/2, holeSize/2, $fn=30);
		}
	}
}


module twisted(x = 0, y = 0, length = 30){
	translate ([x, y]){
		difference (){
			translate ([-length/2, -(tabWidth+gapBuffer*2)/2]) cube ([length,tabWidth+gapBuffer*2,holeSize+holeBuffer*2]);
			translate([-length/2-1,tabWidth/-2, -0.5]) cube ([tabLength+1+tabExtra,tabWidth, holeSize+holeBuffer*2+1]);
			translate([-length/2+tabLength/2,-tabWidth-1, (holeSize+holeBuffer*2)/2]) rotate ([-90 ,0]) cylinder (tabWidth+gapBuffer*2+2, holeSize/2, holeSize/2, $fn=30);
            translate([-length/2+tabLength+2*gapBuffer,gapBuffer*2,-gapBuffer+1/2]) rotate([90,0,0]) cube([length+1,gapBuffer+1,holeSize+holeBuffer*2+1]);
   
      translate([-length/2+tabLength+2*gapBuffer,-gapBuffer*2,gapBuffer*4-1/2]) rotate([-90,0,0]) cube([length+1,gapBuffer,holeSize+holeBuffer*2+1]);
		}
		
			difference(){
				translate([length/2,(tabWidth+gapBuffer*2)/2, (holeSize+holeBuffer*2)/2-tabWidth/2]) rotate ([90,0]) cube ([tabLength,tabWidth, tabWidth+gapBuffer*2]); 
				translate([length/2+tabLength/2,0, 0]) cylinder (tabWidth+gapBuffer*2+2, holeSize/2, holeSize/2, $fn=30);
			}
}	
	
}

//twisted(0,10, 10);
//straight(0,0, 40);
/*twisted(30,10, 20);
straight(30,0, 20);
twisted(0,-10, 20);
twisted(30,-10, 20);*/