# Illumination

The illumination of BM2 and BM3 is based on [OpenFlexure illumination](https://openflexure.org/projects/microscope/docs/3_illumination.html)

To assembly the illumination you need:

- [Condenser lens](https://openflexure.org/projects/microscope/docs/parts/optics/condenser_lens.html) 

- 5mm led light

- [Arm](/Parts/Illumination/Stl/base_fix_tall.stl)    
  [Condenser](/Parts/Illumination/Stl/condenser3mmv1.stl)    
  [Coupling](/Parts/Illumination/Stl/acople_fix3.stl)
  
  

You can find the Openscad files [here](Parts/Illumination).
