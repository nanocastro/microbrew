# BM3_SportCam_M12lens

This setup is a combination of a Drograce 350WP sportcam, the Flypi manipulator, a cylindrical adaptor for M12 lenses  and the OpenFlexure illumination. 

![BM2](/Early_prototypes/BM3_SportCam_M12lens/Images/ValdiviaTest/BM3_SportCam_M12lens.png)

### BOM

| **Part**         | **Component**                                   | **Link**                               |
|:----------------:|:-----------------------------------------------:|:--------------------------------------:|
| Camera           | Drograce 350WP                                  | [Cameras](/Early_prototypes/Parts/cameras.md)           |
| Optics           | M12 lens of different focal distance            | [Optics](/Early_prototypes/Parts/optics.md)             |
| Illumination     | adaptation from OpenFlexure                     | [Illumination](/Early_prototypes/Parts/Illumination.md) |
| Manipulator/base | adaptation from Flypi to test different cameras | [Manipulator](/Early_prototypes/Parts/Manipulator.md)   |
| M12 lens holder  | cylindrical M12 lens adaptor                    |                                        |

### Images

The images below correspond to an experiment with several M12 lenses.

#### During DIY Microscopy for brewers workshop (Valdivia/Chile)

Breadyeast - M12 - 2.5mm

![BM2](/Early_prototypes/BM3_SportCam_M12lens/Images/ValdiviaTest/BM_Drograce250_M12-2.5_breadyeast2.jpg)

Breadyeast - M12 - 3.6mm

![BM2](/Early_prototypes/BM3_SportCam_M12lens/Images/ValdiviaTest/BM_Drograce250_M12-3.6_breadyeast.jpg)

Breadyeast - M12 - 6mm

![BM2](/Early_prototypes/BM3_SportCam_M12lens/Images/ValdiviaTest/BM_Drograce250_M12-6_breadyeast.jpg)


