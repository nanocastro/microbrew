# BM1_C270_RMS-40x-160mm

This setup is based on the optics of the [high resolution version of the OpenFlexure](https://openflexure.org/projects/microscope/docs/2b_high_resolution_optics_module.html) for the Logitech C270 webcam.
<img src="/Early_prototypes/BM1_C270_RMS-40x-160mm/Images/bm1_c270_rms-40x-160_optics.jpg" width="400"/> <img src="/Early_prototypes/BM1_C270_RMS-40x-160mm/Images/bm1_c270_rms-40x-160_microcube.jpg" width="400"/>

### BOM

| **Part**         | **Component**                                                                       | **Link**                                               |
|:----------------:|:-----------------------------------------------------------------------------------:|:------------------------------------------------------:|
| Camera           | Logitech C270 webcam                                                                | [Cameras](/Early_prototypes/Parts/cameras.md)                           |
| Optics           | Objective-RMS 40x 160mm objective    Focal lens -12.7mm diameter, 50mm focal length | [Optics](/Early_prototypes/Parts/optics.md)                             |
| Illumination     | simple led light                                                                    |                                                        |
| Manipulator/base | based on ucube                                                                      | https://github.com/mdelmans/uCube     Only for Z stage |
| Objective holder | based on OpenFlexure                                                                | [objective holder](/Early_prototypes/BM1_C270_RMS-40x-160mm/stl/objective holder.stl)               |

### Images

![breadyeast](/Early_prototypes/BM1_C270_RMS-40x-160mm/Images/bm1_c270_rms-40x-160_breadyeast.png) ![breadyeast](/Early_prototypes/BM1_C270_RMS-40x-160mm/Images/bm1_c270_rms-40x-160_breadyeast1.png)


