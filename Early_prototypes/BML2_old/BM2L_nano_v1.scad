/*
BrewerMicro project : https://gitlab.com/nanocastro/microbrew
 (c) Fernán Federici , July 2019    
Released under the CERN Open Hardware License       
*/

use <threads.scad>

include <logitech_c270_FF.scad>;// modified from openflexure project
include <logitech_c920.scad>;// 
include <sample_clips.scad>; 

use <threadlib/threadlib.scad>

echo ("threadlib version: ", __THREADLIB_VERSION());
type = "M20x1";
Douter = thread_specs(str(type, "-int"))[2] * 1.2;

specs = thread_specs("M20x1");
echo(specs);

module rosca_base(){
turns_base = 15;
translate([0,0,5])
nut(type, turns_base, Douter);
}

//rosca_base();
//translate([0,0,15]) 
//tubo();
//sample_holder();


// Test rosca del sample_holder
//union() {
//     difference(){
//cylinder(r1=Douter/2,r2=17,h=M12_h+turns_sample);
//cylinder(r=Douter/2-1.6,h=M12_h+turns_sample);
//}
//nut(type, turns_sample, Douter);}

sample_holder();
turns_sample=28;
module sample_holder(){
union(){
difference(){
 union() {
    
cylinder(r1=Douter/2,r2=24,h=M12_h+slide_z);
            translate([ 0.00, 0.00, M12_h ]) minkowski(){
              sphere();
              cube([ slide_x, slide_y/2, slide_z ], center=true); }
        
        translate([ 0.00, 0.00, M12_h]) minkowski(){sphere(); 
    #cube([ slide_x/3, slide_y, slide_z], center=true);         
            }
}

cylinder(r1=Douter/2-1.6,r2=22,h=M12_h+slide_z);

    translate([0,slide_x/2-b/2,-slide_z-1+M12_h]) cylinder(r=1.5,h=15);
             
    translate([0,-slide_x/2+b/2,-slide_z-1+M12_h]) cylinder(r=1.5,h=15);
        
    translate([ 0.00, 0.00, M12_h ]) minkowski(){ sphere(); cube([ 25, 25, slide_z ], center=true); }
    
    #translate([slide_x/4,-slide_y/6,M12_h ])
    trylinder_selftap(3, slide_z*2, center=true);
    
    #translate([-slide_x/4,-slide_y/6,M12_h])
    trylinder_selftap(3, slide_z*2, center=true);
    
    #translate([slide_x/3,-slide_y/6,M12_h ])
    trylinder_selftap(3, slide_z*2, center=true);
    
    #translate([-slide_x/3,-slide_y/6,M12_h ])
    trylinder_selftap(3, slide_z*2, center=true);
    
    #translate([slide_x/2-slide_z*2,0,M12_h])
    trylinder_selftap(3, slide_z*2, center=true);
    #translate([-slide_x/2+slide_z*2,0,M12_h])
    trylinder_selftap(3, slide_z*2, center=true);
}
}
translate([0,0,-turns_sample])           
nut(type, turns_sample, Douter);
}



module tubo(){
difference(){
turns_b=40;
//scale([0.03,0.03,0])
bolt(type, turns_b);

translate([ 0, 0, turns_b-  M12_ring_h ]) color("red") cylinder(r=(M12_r+corr*2)*1.03,h= M12_ring_h+5);//where M12 lens sits
translate([0,0,turns_b/2])color ("green")cylinder(r=beam_r*1.03,h=turns_b*2,center=true);
}
}


//translate([slide_x/2-slide_z*2,0,M12_h +turns_sample+slide_z])
//sample_clip([0,20,0]);


corr=0.2;// sometimes used for printing imperfections


rosqui_h=40;
wall=1.5;
M12_r=14/2;//external size
tube_thread_ext=M12_r*2+wall*4;
tube_thread_int= beam_r*2;//M12_r*2-wall*2;
tot_len=55;
M12_h=12;
M12_ring_h=4;
slide_y=95;
slide_x=95;
slide_z=2;
base_z=10;
$fn=60;


//translate([ 0.00, 0.00, tot_len + M12_h]) 

//sample_holder();   


//base("c270");//camera_type can be c270 or c920

//translate([ 0.00, 0.00, rosqui_h-corr ]) bm2l_light();


module base(camera_type){ 
        if(camera_type=="c270"){
//             tube();
       c270_camera_mount();}
         
        if(camera_type=="c920") {
            tube();
       c920_camera_mount();}
    }
            
 
   
//corrijo el diametro del cilindro para adaptar a RepRap (PLA)
                

module sample_holder_old(){//to adjust focus
    difference() {
        union() {
        color("green") cylinder(r=M12_r+wall*3+corr,h=rosqui_h-corr,$fn=100);
          translate([ 0.00, 0.00, rosqui_h-corr ]) minkowski(){
              sphere();
              cube([ slide_x, slide_y/2, slide_z ], center=true); }
        
        translate([ 0.00, 0.00, rosqui_h-corr ]) minkowski(){sphere(); 
    #cube([ slide_x/3, slide_y, slide_z], center=true); 
}
              
        }
        translate([ 0.00, 0.00, -  corr ]) color ("lightblue") english_thread (diameter=(tube_thread_ext+3*corr)/25.4, threads_per_inch=32, length=(rosqui_h*2+corr)/25.4,internal=true, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);
        
    #translate([0,slide_x/2-b/2,-slide_z-1+rosqui_h-corr ]) cylinder(r=1.5,h=15);
             #translate([0,-slide_x/2+b/2,-slide_z-1+rosqui_h-corr ]) cylinder(r=1.5,h=15);
        
    #translate([ 0.00, 0.00, rosqui_h-corr+slide_z*3/2 ]) minkowski(){
              sphere();
              cube([ 25, 25, slide_z ], center=true); }
        
    }}



    
module tube(){//
difference() {
    union() {
        translate([ 0.00, 0.00, tot_len/3 ]) color("red") english_thread (diameter=(((tube_thread_ext)-(corr*6))/25.4), threads_per_inch=32, length=tot_len*2/3/25.4,internal=false, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);//external thread
           cylinder(r=tube_thread_ext/2,h=tot_len*1/3);

    }
   union(){
          translate([ 0.00, 0.00, -  corr ]) color ("lightblue") english_thread (diameter=(tube_thread_int)/25.4, threads_per_inch=32, length=(tot_len*2+corr)/25.4,internal=true, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);//internal thread to reduce reflection
       translate([ 0.00, 0.00, tot_len-  M12_ring_h ]) color("yellow") cylinder(r=M12_r+corr*2,h= M12_ring_h);//where M12 lens sits
   }
      
    
}}

use <./illumination.scad>;
include <./microscope_parameters.scad>;
specs = thread_specs("G2 1/2-ext");


////// Iluminación BM2L (condenser openflexure)//////

/// no ha quedado parametrico aun !!!!///
light_dist=20;
col_base=10;
lens_assembly_z=0;
base_r=11;
//bm2l_light();

module light_arm(){
difference(){
union(){ 
translate([-col_base/2,slide_x/2-col_base,0])cube([col_base,col_base,light_dist+slide_z*2]);
translate([-col_base/2,base_r/2,light_dist+slide_z+lens_assembly_z+d])
cube([col_base,slide_x/2-base_r/2,col_base]);
}
translate([0,slide_x/2-col_base/2,slide_z*2])
rotate([90,0,180])
nuttrap();
translate([0,slide_x/2-col_base/2,-slide_z/2])#cylinder(d=a+corr,h=slide_z*5);
}
}

module bm2l_light(){
translate([ 0.00, 0.00, light_dist+slide_z+lens_assembly_z+d])
rotate([180,0,0]) tall_condenser();
light_arm();
    mirror([0,1,0]){light_arm();}
}


//Set Screw Diameter
Set_Screw_Diameter=2.9; //[2:10]
//Set Screw Nut Diameter
Set_Screw_Nut_Diameter=5.5; //[4:20]
//Set Screw Nut Thickness
Set_Screw_Nut_Thickness=2; //[2:10]

a=Set_Screw_Diameter; //[2:10]
//Set Screw Nut Diameter
b=Set_Screw_Nut_Diameter; //[4:20]
//Set Screw Nut Thickness
c=Set_Screw_Nut_Thickness; //[2:10]

module nuttrap(){
translate([0,(c+1)/2,0])rotate([90,0,0])hexagon(c+1,(b+1)/2);
translate([0,0,(b*3)/2])cube([b+1,c+1,b*3],center = true);
}

module reg_polygon(sides,radius)
{
  function dia(r) = sqrt(pow(r*2,2)/2);  //sqrt((r*2^2)/2) if only we had an exponention op
  if(sides<2) square([radius,0]);
  if(sides==3) triangle(radius);
  if(sides==4) square([dia(radius),dia(radius)],center=true);
  if(sides>4) circle(r=radius,$fn=sides);
}

module hexagonf(radius)
{
  reg_polygon(6,radius);
}

module hexagon(height,radius) 
{
  linear_extrude(height=height) hexagonf(radius);
}
