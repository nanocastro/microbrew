/*
BrewerMicro project : https://gitlab.com/nanocastro/microbrew
 (c) Fernán Federici , July 2019    
Released under the CERN Open Hardware License       
*/

use <threads.scad>

include <logitech_c270_FF.scad>;// modified from openflexure project
include <logitech_c920.scad>;// 



corr=0.2;// sometimes used for printing imperfections


rosqui_h=40;
wall=1.5;
M12_r=14/2;//external size
tube_thread_ext=M12_r*2+wall*4;
tube_thread_int= beam_r*2;//M12_r*2-wall*2;
tot_len=55;
M12_h=12;
M12_ring_h=4;
slide_y=95;
slide_x=95;
slide_z=2;
base_z=10;
$fn=60;


//translate([ 0.00, 0.00, tot_len + M12_h]) 
sample_holder();   


base("c920");//camera_type can be c270 or c920

    
module base(camera_type){ 
        if(camera_type=="c270"){
            translate([30,0,0]) 
            tube();
//       c270_camera_mount();
            }
         
        if(camera_type=="c920") {
            tube();
//       c920_camera_mount();
            }
    }
            
  



module sample_holder(){//to adjust focus
    difference() {
        union() {
        color("green") cylinder(r=M12_r+wall*3,h=rosqui_h-corr,$fn=100);
          translate([ 0.00, 0.00, rosqui_h-corr ]) minkowski(){
              sphere();
              cube([ slide_x, slide_y, slide_z ], center=true); }
        }
        translate([ 0.00, 0.00, -  corr ]) color ("lightblue") english_thread (diameter=(tube_thread_ext)/25.4, threads_per_inch=32, length=(rosqui_h*2+corr)/25.4,internal=true, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);
    }}



    
module tube(){//
difference() {
    union() {
//        translate([ 0.00, 0.00, tot_len/3 ]) color("red") english_thread (diameter=(((tube_thread_ext)-(corr*3))/25.4), threads_per_inch=32, length=tot_len*2/3/25.4,internal=false, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);//external thread
           #cylinder(r=tube_thread_ext/2,h=tot_len*1/3);

    }
   union(){
//          translate([ 0.00, 0.00, -  corr ]) color ("lightblue") english_thread (diameter=(tube_thread_int)/25.4, threads_per_inch=32, length=(tot_len*2+corr)/25.4,internal=true, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);//internal thread to reduce reflection
       #translate([ 0.00, 0.00, tot_len-  M12_ring_h ]) color("yellow") cylinder(r=M12_r+corr*2,h= M12_ring_h);//where M12 lens sits
   }
      
    
}}

