/*
BrewerMicro project : https://gitlab.com/nanocastro/microbrew
 (c) Fernán Federici , July 2019    
Released under the CERN Open Hardware License       
*/

use <threads.scad>

include <logitech_c270_FF.scad>;// modified from openflexure project
include <logitech_c920.scad>;// 

use <threadlib/threadlib.scad>

echo ("threadlib version: ", __THREADLIB_VERSION());

module rosca_base(){
type = "M20x1";
turns = 8;
Douter = thread_specs(str(type, "-int"))[2] * 1.2;
translate([0,0,5])
nut(type, turns, Douter);
}

rosca_base();

corr=0.2;// sometimes used for printing imperfections


rosqui_h=40;
wall=1.5;
M12_r=14/2;//external size
tube_thread_ext=M12_r*2+wall*4;
tube_thread_int= beam_r*2;//M12_r*2-wall*2;
tot_len=55;
M12_h=12;
M12_ring_h=4;
slide_y=95;
slide_x=95;
slide_z=2;
base_z=10;
$fn=60;


//translate([ 0.00, 0.00, tot_len + M12_h]) 

//sample_holder();   


base("c270");//camera_type can be c270 or c920

//translate([ 0.00, 0.00, rosqui_h-corr ]) bm2l_light();


module base(camera_type){ 
        if(camera_type=="c270"){
//             tube();
       c270_camera_mount();}
         
        if(camera_type=="c920") {
            tube();
       c920_camera_mount();}
    }
            
 
   
//corrijo el diametro del cilindro para adaptar a RepRap (PLA)
                

module sample_holder(){//to adjust focus
    difference() {
        union() {
        color("green") cylinder(r=M12_r+wall*3+corr,h=rosqui_h-corr,$fn=100);
          translate([ 0.00, 0.00, rosqui_h-corr ]) minkowski(){
              sphere();
              cube([ slide_x, slide_y/2, slide_z ], center=true); }
        
        translate([ 0.00, 0.00, rosqui_h-corr ]) minkowski(){sphere(); 
    #cube([ slide_x/3, slide_y, slide_z], center=true); 
}
              
        }
        translate([ 0.00, 0.00, -  corr ]) color ("lightblue") english_thread (diameter=(tube_thread_ext+3*corr)/25.4, threads_per_inch=32, length=(rosqui_h*2+corr)/25.4,internal=true, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);
        
    #translate([0,slide_x/2-b/2,-slide_z-1+rosqui_h-corr ]) cylinder(r=1.5,h=15);
            #translate([0,-slide_x/2+b/2,-slide_z-1+rosqui_h-corr ]) cylinder(r=1.5,h=15);
        
    #translate([ 0.00, 0.00, rosqui_h-corr+slide_z*3/2 ]) minkowski(){
              sphere();
              cube([ 25, 25, slide_z ], center=true); }
        
    }}



    
module tube(){//
difference() {
    union() {
        translate([ 0.00, 0.00, tot_len/3 ]) color("red") english_thread (diameter=(((tube_thread_ext)-(corr*6))/25.4), threads_per_inch=32, length=tot_len*2/3/25.4,internal=false, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);//external thread
           cylinder(r=tube_thread_ext/2,h=tot_len*1/3);

    }
   union(){
          translate([ 0.00, 0.00, -  corr ]) color ("lightblue") english_thread (diameter=(tube_thread_int)/25.4, threads_per_inch=32, length=(tot_len*2+corr)/25.4,internal=true, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);//internal thread to reduce reflection
       translate([ 0.00, 0.00, tot_len-  M12_ring_h ]) color("yellow") cylinder(r=M12_r+corr*2,h= M12_ring_h);//where M12 lens sits
   }
      
    
}}

use <./illumination.scad>;
include <./microscope_parameters.scad>;

////// Iluminación BM2L (condenser openflexure)//////

/// no ha quedado parametrico aun !!!!///
light_dist=30;
col_base=10;
lens_assembly_z=0;
//translate([ 0.00, 0.00, rosqui_h-corr ]) bm2l_light();
module bm2l_light(){
difference(){
union(){ 
translate([ 0.00, 0.00, light_dist+slide_z+lens_assembly_z+d])
rotate([180,0,0]) tall_condenser();

translate([-col_base/2,slide_x/2-col_base,0])cube([col_base,col_base,light_dist]);

translate([-col_base/2,slide_x/2-col_base,light_dist/2+slide_z])rotate([60,0,0]) cube([col_base,col_base,light_dist+slide_z*3]);
}
translate([0,slide_x/2-b/2,slide_z*3])rotate([90,0,180])nuttrap();
#translate([0,slide_x/2-b/2,-slide_z-1]) cylinder(r=1.5,h=15);
translate([0,0,slide_z*1.5+40]) cube([ slide_x/3, slide_y, slide_z], center=true); 
translate([-col_base/2,slide_x/2,light_dist/2+slide_z*4])rotate([60,0,0]) cube([col_base+2,col_base+2,light_dist+col_base]);
}
mirror([0,180,0]){
difference(){
union(){
translate([ 0.00, 0.00, light_dist+slide_z+lens_assembly_z+d])
rotate([180,0,0]) tall_condenser();

translate([-col_base/2,slide_x/2-col_base,0])cube([col_base,col_base,light_dist]);

translate([-col_base/2,slide_x/2-col_base,light_dist/2+slide_z])rotate([60,0,0]) cube([col_base,col_base,light_dist+slide_z*3]);
}
translate([0,slide_x/2-b/2,slide_z*3])rotate([90,0,180])nuttrap();
#translate([0,slide_x/2-b/2,-slide_z-1]) cylinder(r=1.5,h=15);
translate([0,0,slide_z*1.5+40]) cube([ slide_x/3, slide_y, slide_z], center=true); 
translate([-col_base/2,slide_x/2,light_dist/2+slide_z*4])rotate([60,0,0]) cube([col_base+2,col_base+2,light_dist+col_base]);

}}}





//Set Screw Diameter
Set_Screw_Diameter=2.9; //[2:10]
//Set Screw Nut Diameter
Set_Screw_Nut_Diameter=5.5; //[4:20]
//Set Screw Nut Thickness
Set_Screw_Nut_Thickness=2; //[2:10]

a=Set_Screw_Diameter; //[2:10]
//Set Screw Nut Diameter
b=Set_Screw_Nut_Diameter; //[4:20]
//Set Screw Nut Thickness
c=Set_Screw_Nut_Thickness; //[2:10]

//nuttrap();
module nuttrap(){
translate([0,(c+1)/2,0])rotate([90,0,0])hexagon(c+1,(b+1)/2);
translate([0,0,(b*3)/2])cube([b+1,c+1,b*3],center = true);
}

module reg_polygon(sides,radius)
{
  function dia(r) = sqrt(pow(r*2,2)/2);  //sqrt((r*2^2)/2) if only we had an exponention op
  if(sides<2) square([radius,0]);
  if(sides==3) triangle(radius);
  if(sides==4) square([dia(radius),dia(radius)],center=true);
  if(sides>4) circle(r=radius,$fn=sides);
}

module hexagonf(radius)
{
  reg_polygon(6,radius);
}

module hexagon(height,radius) 
{
  linear_extrude(height=height) hexagonf(radius);
}
