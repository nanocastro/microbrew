# Prototipos

Todos los prototipos se basan en [hackear una cámara web](https://hackteria.org/wiki/DIY_microscopy) dando vuelta el objetivo para magnificar la imagen.  
Hicimos pruebas observando levaduras en cámara Neubauer.  
El manipulador y la base están diseñados en OpenScad e impresos en 3D.  
Para tener una mejor idea de lo que implica mirar levaduras al microscopio se puede consultar el [material educativo del IPATEC](https://ipatec.conicet.gov.ar/presentaciones/)   

## Prototipo 1
Basado en OpenFlexure con cámara weby objetivo de 40X.   
Eje Z basado en (μcube) https://hackaday.io/project/12777-cube
Luz led en brazo articulado.  

<img src="/Early_prototypes/Images/20190323_083351.jpg" width="400"/> <img src="/Early_prototypes/Images/20190125_211040.jpg" width="400"/>

## Prototipo 2 
Cámara web China. Es la misma utilizada en el [Community microscope de Public Lab](https://store.publiclab.org/collections/featured-kits/products/microscope?variant=8179759612011)  
Manipulador de Flypi modificado.   
Luz led en brazo articulado.  

<img src="/Early_prototypes/Images/20190320_171323.jpg" width="400"/> <img src="/Early_prototypes/Images/vlcsnap-2019-03-22-19h51m02s258.png" width="400"/>

## Prototipo 3
Cámara web Logitech C270 con lente M12 de distancia focal 2.5mm.   
Manipulador Flypi mmodificado.   
Luz led en brazo articulado.  

<img src="/Early_prototypes/Images/IMG_4110.JPG" width="400"/> <img src="/Early_prototypes/Images/vlcsnap-2019-03-22-20h49m53s581.png" width="400"/>

## Prototipo 4
Samsung Galaxy S4 con lentes M12 de distintas distancias focales  
Se utilizó la base del [Community microscope de Public Lab](https://store.publiclab.org/collections/featured-kits/products/microscope?variant=8179759612011) modificada.  
La luz se puso debajo y el telefono con el lente pegado arriba  

<img src="/Early_prototypes/Images/Cellphone/20190428_115723.jpg" width="400"/> <img src="/Early_prototypes/Images/Cellphone/Image 2019-04-28.jpeg" width="400"/>

<img src="/Early_prototypes/Images/Cellphone/20190428_113552.jpg" width="400"/> <img src="/Early_prototypes/Images/Cellphone/20190428_113200.jpg" width="400"/>

## Prototipo 5 - BM1_C270_RMS-40x-160mm
[Documentación e imagenes](/Early_prototypes/BM1_C270_RMS-40x-160mm/readme.md)

## Prototipo 6 - BM2_C270_M12lens
[Documentación e imagenes](/Early_prototypes/BM2_C270_M12lens/readme.md)
Este es el modelo precursor de la actual versión

## Prototipo 7 - BM3_SportCam_M12lens
[Documentación e imagenes](/Early_prototypes/BM3_SportCam_M12lens/readme.md)

## Prototipo 8 - BM4_USBWebCam_M12lens
[Documentación e imagenes](/Early_prototypes/BM4_USBWebCam_M12lens/readme.md)

