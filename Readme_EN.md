# BrewerMicro

Microscope to monitor and count yeast for beer and fermentations in general

<img src="/Prototipo_Actual/Imagenes/IMG_20200819_080843.jpg" width="400"/>
<img src="/Prototipo_Actual/Imagenes/50242738831_982df677ed_o.jpg" width="400"/>
<img src="/Prototipo_Actual/Imagenes/50242955947_8535cbd765_o.jpg" width="400"/>
<img src="/Prototipo_Actual/Imagenes/usaf2.jpg" width="400"/>

We aim to develop and characterize low-cost DIY microscopes that brewers can use to see, count and check yeast viability.

<details>
  <summary markdown="span">Brief history of the project</summary>
The project started as follow up activity from the Open Hardware Workshop Valdivia 2018 and the "Ciencia y Cerveza" Workshop held in Valdivia by [IPATEC](https://ipatec.conicet.gov.ar/ciencia-cerveza/) on March 2019, both supported by the Millenium Institute iBio.  
We had a twofold motivation to start this project, first, the concrete need of local beer brewers to go one step forward towards its microbiologic autonomy by easing the observation, counting and evaluation of yeast cells and, secondly, the wish of learn about the adoption of open source hardware tools outside the academic/scientific world.
The development followed a constant collaboration and prototyping methodology that made use of workshops as a way of engaging, as soon as possible, potencial users and collaborators in the process of co-creation. We have made workshops in Valdivia, Bariloche and Mendoza. We are now preparing an on-line workshop for people from diverse profiles from around Chile.
An important milestone in this process has been the adoption of some ideas that emerged from this process by IPATEC for their [primer versión de microscopio](/Prototipo%20IPATEC%201.0).  
</details>

Our designs are hybrids based on previous open-source microscopes:  

<details>
  <summary markdown="span">Otros microscopios open-source</summary>

- [DIY microscope](https://hackteria.org/wiki/DIY_microscopy)     
- [Openflexure](https://github.com/rwb27/openflexure_microscope)        
- [Flypi](https://github.com/amchagas/Flypi)  
- [Fluopi](https://osf.io/dy6p2/)  
- [Comunicaciones Especulativas](http://interspecifics.cc/comunicacionesespeculativas/)  
</details>

## Current prototype

<img src="/Prototipo_Actual/Imagenes/IMG_20200819_081307.jpg" width="600"/>  

This prototype es based on a C270 Logitech webcam and a 3D printed base to hold and focus an M12 lens. This screw focusing design manipulator was inspired on the work of [Tristan-Landin et al. (2019)](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0215114). The illumination is the one of OpenFlexure.  
With this prototype we have been able to see, and clearly distinguish, live yeast cells (unstained) and dead cells (stained with methylene blue).
It can also be used by other fermenters to observe their productions(ej. kombucha, wine, chicha, kefir, kimchi)    
    [More images](https://www.flickr.com/photos/144912655@N02/albums/72157710829687746) 

<details>
  <summary markdown="span">:nut_and_bolt:  If you want to build this prototype</summary>
In this [folder](/Prototipo_Actual) you can find the [build of materials](BOM.csv), the .stl files and a [guide (in spanish)](Guia_ensamblado.md) with some directions for printing and assembly
</details>

<details>
  <summary markdown="span">:microscope: If you want to use this prototype</summary>
Se puede utilizar el microscopio con cualquier software que reconozca la cámara web Logitech C270.      
Esta son algunos softwares sugeridos para pc:

- Linux -> Cheese.  
    From the console you can download and install with:  
    sudo apt-get install cheese  

- Windows-> https://www.logitech.com/es-ar/product/capture

- Mac -> https://www.logitech.com/en-us  

In case you want to use your cell phone as a microscope display, you need:

- OTG to micro USB cable (link) 

- Download [USB Camera App] (https://play.google.com/store/apps/details?id=infinitegra.app.usbcamera)

</details>

<details>
  <summary markdown="span">:wrench: If you want to modify this prototype</summary>
El diseño ha sido realizado con el software libre [OpenScad](www.openscad.org). 
En la carpeta [Openscad] pueden encontrar el archivo fuente del diseño, una [guía](/Prototipo_Actual/Openscad/Readme.md) de cómo está organizado y algunas indicaciones acerca de los parámetros y las librerías utilizadas.
The design has been made with free software [OpenScad](www.openscad.org).
In the folder [Openscad](/Prototipo_Actual/Openscad) you can find the source file of the design, a [guide](/Prototipo_Actual/Openscad/Readme.md) of how it is organized and some indications about the parameters and libraries used.
</details>

## State of develpment

You can check the [issues](https://gitlab.com/nanocastro/microbrew/issues) to know what things we are working on to improve this design.  
We are currently looking to work with local brewers to begin building a common image base to implement automatic cell counting methods.

## Early prototypes

We have evaluated and documented various combinations of cameras, optics, manipulators, and lighting.  
The designs and evaluations of these early prototypes can be found [here](/ Early_prototypes / Prototypes.md)

## How to participate and collaborate?

You can drop us a line at brewermicro2020 at gmail.com   
You can fork this project and start uploading your modifications.

### Workshops

We have conducted several [workshosp](/Workshops) for brewers and general public in Argentina (Bariloche) and Chile (Santiago, Valdivia) to raise collaborators and users.

## Financial support

<details>
  <summary markdown="span">Biomaker Challenge 2019</summary>
The project has been funded by the [Cambridge-Norwich Biomaker Challenge 2019](https://www.biomaker.org/cambridge-norwich-challenge).
[Documentation and reports](/Biomaker2019) of the Biomaker project.
You can also check the documentation at [hackster](https://www.hackster.io/nanocastro/brewermicro-diy-microscope-for-counting-yeast-f78f19)  
We want to thank to Sam Mugford,  Lawrence Percival-Alwyn, Jenny Molloy y Jim Hasseloff for their precious help during the development of the project.  
</details>
<details>
  <summary markdown="span">iBio</summary>
Instituto Milenio iBio – Iniciativa Científica Milenio MINECON
</details>

> > > 
> > 
> > This project is a collaboration between:

- [Fernan Federici - PUC - Chile](https://federicilab.org/about/) 

- [Gudrun Kausel - UACH - Chile](http://www.ciencias.uach.cl/instituto/bioquimica_microbiologia/investigacion/gudrun_kausel.php)

- [Fernando Castro - Independent - Argentina](https://gitlab.com/nanocastro/)

- [Carlos Bertoli - IPATEC - Argentina](https://ipatec.conicet.gov.ar/ciencia-cerveza/)

- you? (everyone is welcome to collaborate on this project!)
  
  > > > 

This project is released under an [open hardware licence](Licencia.txt) from CERN.  
