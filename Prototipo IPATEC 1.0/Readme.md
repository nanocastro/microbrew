# Prototipo de microscopio IPATEC 1.0 

<img src="/Prototipo%20IPATEC%201.0/dispo1.jpeg" width="400"/>
<img src="/Prototipo%20IPATEC%201.0/dispo2.jpeg" width="400"/>
<img src="/Prototipo%20IPATEC%201.0/IMG-20200907-WA0078.jpg" width="800"/>
<img src="/Prototipo%20IPATEC%201.0/dispoNVOleva_3-6_2.jpg" width="800"/>

Este prototipo está basado en el hack de una cámara web que consiste en remover la lente original y reemplazarla por una lente M12.  
Este prototipo utiliza una lente M12 de 3.6mm de distancia focal y todas las piezas pueden ser impresas en 3D.  
En este repositorio pueden encontrar un [archivo stl](/IPATEC1.5.stl) con todas las partes para imprimir y dos imagenes, una con el [despiece](/microBrewscope_v1.5.pdf) y optra con el prototipo [ensamblado](/microBrewscope_v1.5_a.pdf).   

## Lista de materiales
- Camara web Logitech C270
- Lente M12 3.6mm
- Partes impresas


#### Este prototipo ha sido desarrollado por Carlos Bertoli (IPATEC-Bariloche)  
