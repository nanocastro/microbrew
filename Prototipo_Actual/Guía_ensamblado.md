# Guía de impresión y ensamblado

En la [lista de materiales](/Prototipo_Actual/BOM.csv) pueden encontrar una lista con lo necesario para replicar esta versión del microscopio.
Basicamente se necesitan partes impresas y otras no impresas como la cámara y los lentes.

## Impresión de partes

En la carpteta [STL](/Prototipo_Actual/STL) se encuentran los archivos .stl de las partes que necesitan imprimir. 

<img src="/Prototipo_Actual/Imagenes/IMG_20200819_081307_1.jpg" width="600"/>

Todas las piezas están diseñadas para no tener que utilizar soporte.  
Hemos utilizado tanto ABS como PLA y ambas funcionan bien.   
El alto de capa recomendado es de 0.2mm.  

### Prueba de impresión

Les recomendamos primero realizar una prueba de impresión de la rosca que sirve para hacer foco imprimiendo los archivos de prueba para [hembra](/STL/M20_rosca_prueba.stl) y [macho](M20_bolt_prueba.stl). El .stl de la hembra se imprime tal cual está pero el del macho (bolt) se **escala en las dimensiones x e y** a para permitir su facil montaje. Pueden probar escalando a un 98% y dependiendo del resultado modificarlo para que haya buen deslizamiento pero que no haya juego entre macho y hembra.

## Ensamblado de partes

### Base y tubo óptico

Neecesitamos:  
- Partes impresas
	- [base](STL/BM_base.stl)
	- [tubo óptico](STL/BM_tube.stl)  
- Partes no impresas
	- [Lente M12 2.8mm](/Early_prototypes/Parts/optics.md)
    - [Cámara web Logitech C270](/Early_prototypes/Parts/cameras.md)
- Herramientas
	- Alicate
	- Cutter
	- destornilladores philiphs para electrónica 
    - pistola de silicona

Lo primero que tenemos que hacer es colocar el lente M12 en uno de los extremos del tubo óptico y roscar el otro extremo en en la base. 

<img src="/Prototipo_Actual/Imagenes/IMG_20200819_081031.jpg" width="400"/>

Luego necesitamos desarmar la camara web y remover con **mucho cuidado** la lente que trae la camara para utilizar su detector con una la lente M12 de 2.8mm que colocamos en el tubo óptico. Para ello pueden seguir esta [guía](Hack_C270.pdf) elaborada por Richard Bowmann para el proyecto Openflexure. La única diferencia es que aqui colocaremos la cámara en nuestra base. Recomendamos hacer esto en un lugar limpio y con extrema delicadeza ya que cualquier particula puede dañar el detector de la cámara.

<img src="/Prototipo_Actual/Imagenes/IMG_20200819_080907.jpg" width="400"/>

La cámara se fija a la base con los mismos tornillos y recomendamos pegar el cable de la camara a la base con una gota de silicona. Para impedir que la base se deslice se pueden colocar 1 gota de silicona en cada esquina de la base.

### Etapa e iluminación
Necesitamos
- Partes impresas
	- [Etapa](/STL/BM_sample_holder.stl)
    - [Puente iluminación](STL/BM_light_bearing.stl)
    - 2 [Clips para muestra](STL/BM_sample_clips.stl)
- Partes no impresas
	- [Lente condensador 13mm](https://es.aliexpress.com/item/32242266574.html)
    - [Ruleman 685zz](http://cimech3d.cl/producto/685zz-rodamiento-de-bolas/)
    - 4 tornillos M3x10mm
    - 4 tuercas M3
    - Led blanco 5mm
    - Resistencia 220 Ohm
    - Fuente alimentación 5V o cable USB
 - Herramientas
 	- Soldador de estaño
    - Destornillador philiphs
    - cinta aisladora
  
  El puente de iluminación cuenta con 2 aberturas, en la superior colocamos el ruleman y en la inferior el lente que servirá de condensador. Podemos fijar el lente con un punto de pegamento si fuera necesario.
  Luego necesitamos soldar una resistencia a la pata positiva del LED y luego unir el Led al cable de alimentación. Se puede cortar y soldar un cable USB viejo o  se puede usar un jack hembra con terminal y una fuente de 5V. 

<img src="/Prototipo_Actual/Imagenes/jack.jpeg" width="400"/>

Una vez armado el canble introducimos el led en el orificio del ruleman y lo fijamos con una gotita de pegamento, Luego atornillamos el puente de iluminación a la etapa y roscamos la etapa al tubo óptico. Por último, atornillamos los clips a la etapa y eso es todo amigues!!  

<img src="/Prototipo_Actual/Imagenes/luzyetapa.jpeg" width="400"/>
<img src="/Prototipo_Actual/Imagenes/etapayluz.jpeg" width="400"/>


<img src="/Prototipo_Actual/Imagenes/IMG_20200819_080843.jpg" width="600"/>


