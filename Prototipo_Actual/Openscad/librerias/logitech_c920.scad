/******************************************************************
 This code is an adaption of the module "logitech_c920.scad" (written by the OpenFlexure Microscope team).

This file defines a camera mount for the Logitech  c920 webcam.                                  (c) Fernan Federici, January 2019  Released under the CERN Open Hardware License                   

******************************************************************/


use <utilities.scad>;//from openflexure
beam_r=6; //this radius should be the same used in base.scad module to match the tube hole with this hole
beam_h=13;
        PCB_h=5;//height (depth) of celarance for PCB
        PCB_y=81;
        PCB_x=22.5;
    L = 100;//58;
    w = 75;//25;
    
$fn=48;
d=0.05;


function c920_camera_mount_height() = 4.5;//if you modified it here, the  full cutout moves downward
bottom = c920_camera_mount_height() * -1;
function c920_camera_sensor_height() = 0.2; //Height of the sensor above the PCB

module mounting_hole(){
   color("red") translate([0,0,-5]) cylinder(r=0.8*1.2,h=999,$fn=12); //screw holes
    translate([0,0,-0.5]) cylinder(r1=0.8*1.2,h=1,r2=0.8*1.2+1,$fn=12);
}



//C920();

//c920_camera_mount();
module C920(){
    //cut-out to fit logitech c920 webcam
    //optical axis at (0,0)
    //top of PCB at (0,0,0)
    mounting_hole_x = 8.25;//half of distance between screw holes for the lens holder
    mirror([0,0,1]){ //makes the full cutout upside down 
        
        //beam clearance facing the sensor
       hull(){
           cube([9,9,6],center=true);
            translate([0,0,-beam_h]) cylinder(r=beam_r,h=2*d,center=true);
        }

        //mounting holes
       rotate(90,0,0) reflect([1,0,0]) translate([mounting_hole_x,0,0]) mounting_hole(); 
        
        //clearance for PCB
           color("cyan") 
                translate([0,0,PCB_h/2]) cube([PCB_x,PCB_y,PCB_h], center=true);

            color("yellow") reflect([0,1,0]) 
               translate([0,mounting_hole_x+1,1]) cube([PCB_x,7.5,PCB_h], center=true);
                
           
            //space to house the pin and wire connector on PCB 
            pin_house_x=PCB_x;
            pin_house_y=PCB_y/2-mounting_hole_x;
            pin_house_h=12;//depth of the space housing pin and wires
        
             color("red")    reflect([0,1,0]){ translate([0,pin_house_y/2+mounting_hole_x+3,-pin_house_h/2]) cube([pin_house_x,pin_house_y,pin_house_h],center=true);}
            
        
        
        //exit for cable, microbrew team made it bigger to be able to enlarge base while leaving the cutout clear
       hull(){
           translate([4,20,0]) rotate([-90,0,0]) cylinder(r=3,h=99);
           translate([4,20,3]) rotate([-90,0,0]) cylinder(r=3,h=99);
       }
        
    }
}

module c920_camera_mount(){
    // A mount for the pi camera v2
    // This should finish at z=0+d, with a surface that can be
    // hull-ed onto the lens assembly.

    difference(){
hull(){
    translate([-w/2, -L/2, bottom*2]) minkowski(){
        sphere();
        cube([w, L, c920_camera_mount_height()*3.6]);
    }
    cylinder(r=15/2, h=5);
}
        translate([0,0,-(1+ c920_camera_mount_height()*2)]) cube([w, L,2], center=true);

//        translate([-w/2, -L/2, bottom*2]) cube([w, L, c920_camera_mount_height()*2]);
       translate([0,0,bottom]) C920();

    
}
}




