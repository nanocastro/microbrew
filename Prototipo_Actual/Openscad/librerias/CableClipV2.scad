// updates: you only need to set the diameter of your cable and the loop count

d_clip=4;  // cable diameter - probably set it a bit larger than the actual value
n=1;    // cable loop count

nx=1;   // how many you want
ny=1;   // 2 * 4 = 8 pcs

d1=d_clip/2;
d2=(3/4)*d_clip;
b_clip=2*d_clip;
l=(n+0.9)*d_clip;
h=20;

e=0.001;

module outer(l,h,b_clip,d1,d2){
    cylinder(r=b_clip/2,h=h,$fn=24);
    difference(){
        translate([0,-b_clip/2,0])cube([l-b_clip/2,b_clip,h]);
        translate([l-b_clip/2-d2*sin(40),0,-e])rotate([0,0,-40])cube([b_clip,b_clip,h+2*e]);
        translate([l-b_clip/2-d2*sin(40),0,-e])rotate([0,0,-50])cube([b_clip,b_clip,h+2*e]);
    }
}

module inner(l,h,b_clip,d1,d2){
    translate([0,0,-e]){
        for (i=[0:n-1]){
            translate([i*d_clip,0,0])cylinder(r=b_clip/2-d1,h=h+2*e,$fn=24);
        }
        translate([0,-(b_clip-2.6*d1)/2,0])cube([l-b_clip,b_clip-2.6*d1,h+2*e]);
        translate([0,-(b_clip-2*d2)/2,0])cube([l,b_clip-2*d2,h+2*e]);
    }
}

module clip(l,h,b_clip,d1,d2){
    difference(){
        outer(l,h,b_clip,d1,d2);
        inner(l,h,b_clip,d1,d2);
    }
}

//for(xi=[0:nx-1])
//    for(yi=[0:ny-1])
//        translate([xi*(l+1.5),yi*(b_clip+1.5),0])clip(l,h,b_clip,d1,d2);
