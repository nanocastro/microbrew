# Guía para modificar el prototipo

En el archivo [BML2_v2.scad]([BML2_v2.scad) se encuentra el diseño de la versión actual del prototipo y puede ser modificado en todas sus partes cambiando los valores de los distintos parámetros.  

El archivo está organizado por partes. En la primer parte se encuentran las librerías utilizadas, luego se especifican algunos parámetros que pueden ser modificados (cámara, rulemanes, medida de la rosca, etc) y luego los renders. En su estado original el codigo renderiza todas las partes del microscopio pero eso se puede cambiar según la necesidad.  

La librería principal es la utilizada para las roscas y es necesario instalarla para correr el .scad del prototipo. En este [enlace](https://github.com/adrianschlatter/threadlib) pueden encontrar las Instrucciones para su instalación y su uso.  

En la librería de rulemanes se encuentran las medidas de los disntintos rulemanes. Para utilizar alguno que no este contemplado se deben agregar las medidas.  

La iluminación y la base de la cámara están basadas en librerías de Openflexure
https://gitlab.com/openflexure/openflexure-microscope-snesclient






